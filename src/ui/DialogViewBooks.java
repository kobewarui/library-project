/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import libproject.Author;
import java.sql.*;
import javax.swing.JOptionPane;
import libproject.HintTextField;
import libproject.Libproject;

/**
 *
 * @author jakata
 */
public final class DialogViewBooks extends JDialog {

    private JTable table;
    private JComboBox jcomboBox;
    private JButton     buttonSearch,buttonViewBooks;
    private JTextField bookTextField, authorTextfield, isbnNumberTextfield;
    private DefaultTableModel tableModel;
    private ArrayList<Author> listAuthors;
    private final String filePath = "C:\\Users\\jakata\\Desktop\\authors.txt";
    private HintTextField htfSearch;

    public DialogViewBooks(String title, JFrame parent) {
        super(parent, true);
        initComponents();
        List<Author> authors = loadAuthorsFromDatabase();
        for (Author a : authors) {
            tableModel.addRow(new Object[]{a.getSurname(), a.getLastName(), a.getPhoneNumber()});
        }
        setSize(700, 500);
        setResizable(false);
        setTitle(title);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    private void initComponents() {
        setLayout(new BorderLayout());
        //we already know that our dialog has a BorderLayout
        JPanel panelSearch = new JPanel(new BorderLayout());
        panelSearch.setBorder(new EmptyBorder(5, 5, 5, 5));
        panelSearch.setBackground(Color.MAGENTA);
        HintTextField htfSearch = new HintTextField("Search Author");
        panelSearch.add(htfSearch, BorderLayout.CENTER);
        buttonSearch = new JButton("Search Author");
        buttonViewBooks = new JButton("View Books");
        panelSearch.add(buttonSearch, BorderLayout.EAST);
        add(panelSearch, BorderLayout.NORTH);
        //create table
        tableModel = new DefaultTableModel(new Object[][]{}, new String[]{"Surname", "Last Name", "Phone Number"});
        table = new JTable(tableModel);
        table.setRowHeight(22);
        table.setFillsViewportHeight(true);
        table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        add(new JScrollPane(table), BorderLayout.CENTER);
        //add bottom panel
        JPanel panelActions = new JPanel(new FlowLayout());
        panelActions.add(buttonViewBooks);

        add(panelActions, BorderLayout.SOUTH);

       buttonViewBooks.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int row = table.getSelectedRow();
                if(row==-1){
                JOptionPane.showMessageDialog(DialogViewBooks.this,"Please select an author ");
                return;
                
                }
                DialogBooks dialogBooks = new DialogBooks("Dialog Books", DialogViewBooks.this,listAuthors,row);
                dialogBooks.showBooks();
                dialogBooks.setVisible(true);
            }
       } );

        

        



       

        buttonSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tableModel.setRowCount(0);
                String value = htfSearch.getText();
                try {

                    String query = "SELECT* FROM Author WHERE surname = ? ";

                    Connection connection = Libproject.databaseConnection;
                    PreparedStatement ps = connection.prepareStatement(query);
                    ps.setString(1, value);
                    ResultSet rs = ps.executeQuery();

                    while (rs.next()) {
                        Author author = new Author(rs.getLong("id"), rs.getString("surname"), rs.getString("last_name"), rs.getString("phone"));
                        tableModel.addRow(new Object[]{author.getSurname(), author.getLastName(), author.getPhoneNumber()});

                    }

                } catch (SQLException ex) {
                    Logger.getLogger(DialogManageAuthors.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

    }

    public List<Author> loadAuthorsFromFile() throws IOException, ClassNotFoundException {
        File file = new File(filePath);
        //if this returns true that means the file is empty
        if (file.length() == 0) {

            return new ArrayList<>();
        }
        FileInputStream fis = new FileInputStream(file);

        try {
            listAuthors = new ArrayList<>();
            ObjectInputStream ois = new ObjectInputStream(fis);
            while (fis.available() > 0) {
                Author author = (Author) ois.readObject();
                listAuthors.add(author);
            }
            ois.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(DialogAddBook.class
                    .getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(DialogAddBook.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listAuthors;
    }

    public List<Author> loadAuthorsFromDatabase() {
        listAuthors = new ArrayList<>();
        try {

            String query = "SELECT id,surname,last_name,phone FROM AUTHOR";
            Connection connection = Libproject.databaseConnection;
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                Author author = new Author(rs.getLong("id"), rs.getString("surname"), rs.getString("last_name"), rs.getString("phone"));
                listAuthors.add(author);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DialogManageAuthors.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return listAuthors;

    }

}
