/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import libproject.Author;
import libproject.MyObjectOutputStream;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import javax.swing.BorderFactory;
import javax.swing.table.DefaultTableModel;
import java.sql.*;
import libproject.Libproject;

public final class DialogAddAuthor extends JDialog implements ActionListener {

    private JButton buttonsSave;
    private JTextField surnameTextField, lastNameTextfield, phoneNumberTextfield;
    private JLabel labelSurname, labelLastName, labelPhonenumber;
    private final DefaultTableModel tableModel;
    ArrayList<Author> authorList;
    private final String filePath = "C:\\Users\\jakata\\Desktop\\authors.txt";

    public DialogAddAuthor(String title, JDialog parent, DefaultTableModel tableModel) {
        super(parent, true);
        this.tableModel = tableModel;
        initComponents();
        setSize(500, 500);
        setResizable(false);
        setTitle(title);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);

    }

    private void initComponents() {
        setLayout(new BorderLayout());
        buttonsSave = new JButton("Save");
        surnameTextField = new JTextField();
        lastNameTextfield = new JTextField();
        phoneNumberTextfield = new JTextField();
        labelSurname = new JLabel();
        labelLastName = new JLabel();
        labelPhonenumber = new JLabel();
        buttonsSave.addActionListener(this);

        JPanel southPanel = new JPanel();
        southPanel.add(buttonsSave);
        add(southPanel, BorderLayout.SOUTH);
        JPanel centerPanel = new JPanel(new GridLayout(0, 1));
        centerPanel.add(new JLabel("Surname"));
        centerPanel.add(surnameTextField);
        centerPanel.add(labelSurname);
        centerPanel.add(new JLabel("Last Name"));
        centerPanel.add(lastNameTextfield);
        centerPanel.add(labelLastName);
        centerPanel.add(new JLabel("Phone Number"));
        centerPanel.add(phoneNumberTextfield);
        centerPanel.add(labelPhonenumber);
        add(centerPanel, BorderLayout.CENTER);
    }

    public void saveAuthorToFile(Author author) {

        //Problem Description:
//Every time you serialize an object to a file, you only want to append the object 
//to the end of the file instead of overwriting it. 
//You can use FileInputStream (file name, true); when reading data, 
//it will be read normally for the first time. It will report an error, and when it is read the second time,
//it will report a java.io.StreamCorruptedException: invalid type code: AC error.
//problem analysis:
//
//Since FileOutputStream (file name, true) is used to serialize objects to the same file,
//a header is serialized to the file each time. When deserializing, 
//each ObjectInputStream object will only read one header, 
//then when encountering the second one, an error will be reported, resulting in an exception.
        try {
            File file = new File(filePath);
            FileOutputStream fos = new FileOutputStream(file, true);
            //If there is nothing on the file write a new streamheader
            if (file.length() < 1) {
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(fos);
                objectOutputStream.writeObject(author);
                objectOutputStream.close();

// Do not write a new stream header
            } else {
                MyObjectOutputStream mos = new MyObjectOutputStream(fos);
                mos.writeObject(author);
                mos.close();

            }

            //
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String surname = surnameTextField.getText();
        String lastName = lastNameTextfield.getText();
        String phoneNumber = phoneNumberTextfield.getText();

        if (!surname.matches("(?i)(^[a-z]+)((?![ .,'-]$)[a-z .,'-]){0,24}$")) {

            surnameTextField.setBorder(BorderFactory.createLineBorder(Color.RED));
            labelSurname.setForeground(Color.red);
            labelSurname.setText("Invalid name");
            return;

        } else {

            System.out.println("This surname is valid ");
            surnameTextField.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
            labelSurname.setForeground(Color.BLACK);
            labelSurname.setText("valid");

            if (!lastName.matches("(?i)(^[a-z]+)((?![ .,'-]$)[a-z .,'-]){0,24}$")) {

                lastNameTextfield.setBorder(BorderFactory.createLineBorder(Color.RED));
                labelLastName.setForeground(Color.red);
                labelLastName.setText("Invalid last Name");
                return;

            } else {

                System.out.println("This lastname is valid ");
                lastNameTextfield.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
                labelLastName.setForeground(Color.BLACK);
                labelLastName.setText("valid");

                if (!phoneNumber.matches("\\d{10}")) {

                    phoneNumberTextfield.setBorder(BorderFactory.createLineBorder(Color.RED));
                    labelPhonenumber.setForeground(Color.red);
                    labelPhonenumber.setText("Invalid phone Number");
                    return;

                } else {

                    System.out.println("This Number is valid ");
                    phoneNumberTextfield.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
                    labelPhonenumber.setForeground(Color.BLACK);
                    labelPhonenumber.setText("valid");
                    tableModel.addRow(new Object[]{surname, lastName, phoneNumber});
                }
            }

        }
        //validateUserInput(author);        
        Author author = new Author(0,surname, lastName, phoneNumber);
        saveAuthorToFile(author);
        saveAuthorToDatabase(author);

        this.dispose();
    }

  

    private void saveAuthorToDatabase(Author author){
        try {

            String surname = author.getSurname();
            String lastName = author.getLastName();
            String phoneNumber = author.getPhoneNumber();
            String query = "INSERT INTO AUTHOR(surname,last_name,phone) VALUES('" + surname + "','" + lastName + "','" + phoneNumber + "')";
            Connection connection = Libproject.databaseConnection;
            Statement statement = connection.createStatement();
            statement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                long id = rs.getLong(1);
                author.setId(id);

            }

        } catch (SQLException ex) {
            Logger.getLogger(DialogAddAuthor.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
