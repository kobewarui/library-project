///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import libproject.Author;
import libproject.MyObjectOutputStream;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import javax.swing.BorderFactory;
import javax.swing.table.DefaultTableModel;
import java.sql.*;
import java.util.List;
import javax.swing.JTable;
import libproject.Book;
import libproject.Libproject;

public final class DialogEditAuthor extends JDialog implements ActionListener {

    private JButton buttonsAdd;
    private JTextField surnameTextField, lastNameTextfield, phoneNumberTextfield;
    private JLabel labelSurname, labelLastName, labelPhonenumber;
    private final DefaultTableModel tableModel;
    private JTable table;
    ArrayList<Author> authorList;
    private final String filePath = "C:\\Users\\jakata\\Desktop\\authors.txt";
    private String phone;
    private Author editAuthor;

    public DialogEditAuthor(Author author, String title, JDialog parent, DefaultTableModel tableModel, JTable table) {
        super(parent, true);
        this.tableModel = tableModel;
        this.table = table;
        this.editAuthor = author;
        initComponents();
        setSize(500, 500);
        setResizable(false);
        setTitle(title);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);

    }

    private void initComponents() {
        setLayout(new BorderLayout());
        buttonsAdd = new JButton("Save");
        surnameTextField = new JTextField();
        lastNameTextfield = new JTextField();
        phoneNumberTextfield = new JTextField();
        labelSurname = new JLabel();
        labelLastName = new JLabel();
        labelPhonenumber = new JLabel();
        buttonsAdd.addActionListener(this);

        JPanel southPanel = new JPanel();
        southPanel.add(buttonsAdd);
        add(southPanel, BorderLayout.SOUTH);
        JPanel centerPanel = new JPanel(new GridLayout(0, 1));
        centerPanel.add(new JLabel("Surname"));
        centerPanel.add(surnameTextField);
        centerPanel.add(labelSurname);
        centerPanel.add(new JLabel("Last Name"));
        centerPanel.add(lastNameTextfield);
        centerPanel.add(labelLastName);
        centerPanel.add(new JLabel("Phone Number"));
        centerPanel.add(phoneNumberTextfield);
        centerPanel.add(labelPhonenumber);
        add(centerPanel, BorderLayout.CENTER);
    }

    public void saveAuthorToFile(Author author) {

        //Problem Description:
//Every time you serialize an object to a file, you only want to append the object 
//to the end of the file instead of overwriting it. 
//You can use FileInputStream (file name, true); when reading data, 
//it will be read normally for the first time. It will report an error, and when it is read the second time,
//it will report a java.io.StreamCorruptedException: invalid type code: AC error.
//problem analysis:
//
//Since FileOutputStream (file name, true) is used to serialize objects to the same file,
//a header is serialized to the file each time. When deserializing, 
//each ObjectInputStream object will only read one header, 
//then when encountering the second one, an error will be reported, resulting in an exception.
        try {
            File file = new File(filePath);
            FileOutputStream fos = new FileOutputStream(file, true);
            //If there is nothing on the file write a new streamheader
            if (file.length() < 1) {
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(fos);
                objectOutputStream.writeObject(author);
                objectOutputStream.close();

// Do not write a new stream header
            } else {
                MyObjectOutputStream mos = new MyObjectOutputStream(fos);
                mos.writeObject(author);
                mos.close();

            }

            //
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    // This code executes when you click the save button
    public void actionPerformed(ActionEvent e) {
        String surname = surnameTextField.getText();
        String lastName = lastNameTextfield.getText();
        String phoneNumber = phoneNumberTextfield.getText();

        if (!surname.matches("(?i)(^[a-z]+)((?![ .,'-]$)[a-z .,'-]){0,24}$")) {

            surnameTextField.setBorder(BorderFactory.createLineBorder(Color.RED));
            labelSurname.setForeground(Color.red);
            labelSurname.setText("Invalid name");
            return;

        } else {

            System.out.println("This surname is valid ");
            surnameTextField.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
            labelSurname.setForeground(Color.BLACK);
            labelSurname.setText("valid");

            if (!lastName.matches("(?i)(^[a-z]+)((?![ .,'-]$)[a-z .,'-]){0,24}$")) {

                lastNameTextfield.setBorder(BorderFactory.createLineBorder(Color.RED));
                labelLastName.setForeground(Color.red);
                labelLastName.setText("Invalid last Name");
                return;

            } else {

                System.out.println("This lastname is valid ");
                lastNameTextfield.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
                labelLastName.setForeground(Color.BLACK);
                labelLastName.setText("valid");

                if (!phoneNumber.matches("\\d{10}")) {

                    phoneNumberTextfield.setBorder(BorderFactory.createLineBorder(Color.RED));
                    labelPhonenumber.setForeground(Color.red);
                    labelPhonenumber.setText("Invalid phone Number");
                    return;

                } else {

                    System.out.println("This Number is valid ");
                    phoneNumberTextfield.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
                    labelPhonenumber.setForeground(Color.BLACK);
                    labelPhonenumber.setText("valid");

                    Author author = new Author(0,surname, lastName, phoneNumber);
                    saveAuthorToFile(author);
                    updateAuthorToDatabase(author);

                }
            }

        }
        //validateUserInput(editAuthor);        

        this.dispose();
    }

    public void updateDialog() {

        surnameTextField.setText(editAuthor.getSurname());
        phoneNumberTextfield.setText(editAuthor.getPhoneNumber());
        lastNameTextfield.setText(editAuthor.getLastName());

    }

    public void updateAuthorToDatabase(Author author) {
        try {

            Connection connection = Libproject.databaseConnection;

            String surname = author.getSurname();
            String lastName = author.getLastName();
            String phoneNumber = author.getPhoneNumber();
            String query = "UPDATE author SET surname =?,last_name =?,phone =? where id =?";
            PreparedStatement pst = connection.prepareStatement(query);
            pst.setString(1, surname);
            pst.setString(2, lastName);
            pst.setString(3, phoneNumber);
            pst.setLong(4, editAuthor.getId());
            pst.executeUpdate();
            tableModel.setRowCount(0);
            List<Author> authors = loadAuthorsFromDatabase();
            for (Author a : authors) {
                tableModel.addRow(new Object[]{a.getSurname(), a.getLastName(), a.getPhoneNumber()});
            }

        } catch (SQLException ex) {
            Logger.getLogger(DialogAddAuthor.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public List<Author> loadAuthorsFromDatabase() {
        authorList = new ArrayList<>();
        try {

            String query = "SELECT id,surname,last_name,phone FROM AUTHOR";

            Connection connection = Libproject.databaseConnection;
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                Author author = new Author(rs.getLong("id"),rs.getString("surname"), rs.getString("last_name"), rs.getString("phone"));
                authorList.add(author);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DialogManageAuthors.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return authorList;

    }

}
