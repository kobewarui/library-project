/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.BorderLayout;
import java.sql.*;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import libproject.Author;
import libproject.Book;
import libproject.Libproject;
import libproject.MyObjectOutputStream;

/**
 *
 * @author jakata
 */
public final class DialogEditBook extends JDialog implements ActionListener {

    /**
     *
     * @author jakata
     */
    private JButton buttonsSave;
    private JTextField bookTextField, isbnNumberTextfield;
    private JLabel labelTitle, labelIsbnNumber;
    private JComboBox comboBoxAuthorsAuthors;
    private DefaultComboBoxModel comboBoxAuthorsModelAuthors;
    private final DefaultTableModel tableModelBooks;
    private final String filePath = "C:\\Users\\jakata\\Desktop\\authors.txt";
    private List<Author> listAuthors = new ArrayList<>();
    private final Book book;
    private final int rowIndex;

    public DialogEditBook(Book book, String title, JDialog parent, DefaultTableModel tableModel, int rowIndex) {
        super(parent, true);
        this.tableModelBooks = tableModel;
        this.rowIndex = rowIndex;
        this.book = book;
        initComponents();
        setSize(500, 500);
        setResizable(false);
        setTitle(title);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
        System.out.println("book id: "+book.getId());
    }

    private void initComponents() {
        setLayout(new BorderLayout());
        buttonsSave = new JButton("Save");
        bookTextField = new JTextField();
        isbnNumberTextfield = new JTextField();
        labelTitle = new JLabel();
        labelIsbnNumber = new JLabel();
        comboBoxAuthorsModelAuthors = new DefaultComboBoxModel();
        comboBoxAuthorsAuthors = new JComboBox(comboBoxAuthorsModelAuthors);

        listAuthors = loadAuthorsFromDatabase();
        //Author[] array = listAuthors.toArray(new Author[listAuthors.size()]);

        for (int i = 0; i < listAuthors.size(); i++) {
            comboBoxAuthorsModelAuthors.addElement(listAuthors.get(i));
        }
        buttonsSave.addActionListener(this);

        JPanel southPanel = new JPanel();
        southPanel.add(buttonsSave);
        add(southPanel, BorderLayout.SOUTH);
        JPanel centerPanel = new JPanel(new GridLayout(0, 1));
        centerPanel.add(new JLabel("Book Title"));
        centerPanel.add(bookTextField);
        centerPanel.add(labelTitle);
        centerPanel.add(new JLabel("Author"));
        centerPanel.add(comboBoxAuthorsAuthors);
        centerPanel.add(new JLabel("ISBN Number"));
        centerPanel.add(isbnNumberTextfield);
        centerPanel.add(labelIsbnNumber);
        add(centerPanel, BorderLayout.CENTER);

    }

    public void saveBookToFile(Book book) {

        //Problem Description:
//Every time you serialize an object to a file, you only want to append the object 
//to the end of the file instead of overwriting it. 
//You can use FileInputStream (file nameOfSelectedAuthor, true); when reading data, 
//it will be read normally for the first time. It will report an error, and when it is read the second time,
//it will report a java.io.StreamCorruptedException: invalid type code: AC error.
//problem analysis:
//
//Since FileOutputStream (file nameOfSelectedAuthor, true) is used to serialize objects to the same file,
//a header is serialized to the file each time. When deserializing, 
//each ObjectInputStream object will only read one header, 
//then when encountering the second one, an error will be reported, resulting in an exception.
        try {

            File file = new File("C:\\Users\\jakata\\Desktop\\books.txt");
            FileOutputStream fileOutputStream = new FileOutputStream(file, true);

            if (file.length() < 1) {
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
                objectOutputStream.writeObject(book);
                objectOutputStream.close();

            } else {
                MyObjectOutputStream mos = new MyObjectOutputStream(fileOutputStream);
                mos.writeObject(book);
                mos.close();

            }

            //fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void updateBookToDatabase() {
        try {
            Connection connection = Libproject.databaseConnection;

            String bookTitle = book.getTitle();
            String isbn_number = book.getIsbnNumber();
            long authorId = book.getAuthor().getId();
            String query = "UPDATE book SET book_title = ?,isbn_number = ?,author_id = ? where id=?";
            PreparedStatement pst = connection.prepareStatement(query);
            pst.setString(1, bookTitle);
            pst.setString(2, isbn_number);
            //System.out.println(" isbn number,bookTitle,author id = " + isbn_number + bookTitle + authorId);
            pst.setLong(3, authorId);
            pst.setLong(4, this.book.getId());
            pst.executeUpdate();
            //update book details at rowIndex
            tableModelBooks.setValueAt(bookTitle, rowIndex, 0);
            tableModelBooks.setValueAt(book.getAuthor().getSurname() + " " + book.getAuthor().getLastName(), rowIndex, 1);
            tableModelBooks.setValueAt(book.getIsbnNumber(), rowIndex, 2);
        } catch (SQLException ex) {
            Logger.getLogger(DialogAddAuthor.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public List<Author> loadAuthorsFromFile() throws IOException, ClassNotFoundException {

        FileInputStream fis = new FileInputStream(new File(filePath));

        try {
            listAuthors = new ArrayList<>();
            ObjectInputStream ois = new ObjectInputStream(fis);
            while (fis.available() > 0) {
                Author author = (Author) ois.readObject();
                listAuthors.add(author);
            }
            ois.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(DialogAddBook.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DialogAddBook.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            fis.close();

        }
        return listAuthors;
    }

    public List<Author> loadAuthorsFromDatabase() {

        try {

            String query = "SELECT id,surname,last_name,phone FROM AUTHOR";
            Connection connection = Libproject.databaseConnection;
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                long authorId = rs.getLong("id");
                Author author = new Author(authorId, rs.getString("surname"), rs.getString("last_name"), rs.getString("phone"));
                listAuthors.add(author);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DialogManageAuthors.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listAuthors;

    }

    @Override
    // This is called when you click the save Button.
    public void actionPerformed(ActionEvent e) {
        String bookTitle = bookTextField.getText();
        String isbnNumber = isbnNumberTextfield.getText();
        Author selectedAuthor = (Author) comboBoxAuthorsModelAuthors.getSelectedItem();

//        try {
//            long authorId = selectedAuthor.getId();
//
//            String query = "SELECT * FROM Author WHERE id = ? ";
//
//            Connection connection = Libproject.databaseConnection;
//            PreparedStatement ps = connection.prepareStatement(query);
//            ps.setInt(1, (int) authorId);
//            ResultSet rs = ps.executeQuery();
//
//        } catch (SQLException ex) {
//            Logger.getLogger(DialogManageAuthors.class.getName()).log(Level.SEVERE, null, ex);
        //       }
        if (!bookTitle.matches("(?i)(^[a-z]+)((?![ .,'-]$)[a-z .,'-]){0,28}$")) {

            bookTextField.setBorder(BorderFactory.createLineBorder(Color.RED));
            labelTitle.setForeground(Color.red);
            labelTitle.setText("Invalid name");
            return;

        } else {

            System.out.println("This Title is valid ");
            bookTextField.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
            labelTitle.setForeground(Color.BLACK);
            labelTitle.setText("valid");

            if (!isbnNumber.matches("\\d+")) {

                isbnNumberTextfield.setBorder(BorderFactory.createLineBorder(Color.RED));
                labelIsbnNumber.setForeground(Color.red);
                labelIsbnNumber.setText("Invalid Number");
                return;

            } else {

                System.out.println("This Number is valid ");
                isbnNumberTextfield.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
                labelIsbnNumber.setForeground(Color.BLACK);
                labelIsbnNumber.setText("valid");
                book.setTitle(bookTitle);
                book.setAuthor(selectedAuthor);
                book.setIsbnNumber(isbnNumber);
                saveBookToFile(book);
                updateBookToDatabase();
                this.dispose();

            }
        }

    }
    //validateUserInput(author);

    public void updateDialog() {

        String bookTitle = book.getTitle();
        String isbn_number = book.getIsbnNumber();

        bookTextField.setText(bookTitle);
        isbnNumberTextfield.setText(isbn_number);
        comboBoxAuthorsModelAuthors.setSelectedItem(book.getAuthor());

    }

}
