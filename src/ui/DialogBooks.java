/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Frame;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import libproject.Author;
import libproject.Book;
import libproject.HintTextField;
import libproject.Libproject;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jakata
 */
public class DialogBooks extends JDialog {

    DefaultTableModel tableModel;
    JTable table;
    ArrayList<Author> listAuthors;
    int row;

    public DialogBooks(String title, JDialog parent, ArrayList<Author> listAuthors, int row) {
        super(parent, true);
        initComponents();
        this.row = row;
        this.listAuthors = listAuthors;
        setSize(700, 500);
        setResizable(false);
        setTitle(title);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    private void initComponents() {
        setLayout(new BorderLayout());
        JPanel panelSearch = new JPanel(new BorderLayout());
        panelSearch.setBackground(new Color(171, 219, 227));
        panelSearch.setBorder(new EmptyBorder(5, 5, 5, 5));
        HintTextField htfSearch = new HintTextField("Search Author");
        BufferedImage image = null;

        try {
            image = ImageIO.read(new File("C:\\Users\\jakata\\Desktop\\book3.png"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        JLabel picLabel = new JLabel(new ImageIcon(image));
        JLabel stringLabel = new JLabel("Robert Kiyosaki");
        stringLabel.setSize(50, 50);
        panelSearch.add(stringLabel, BorderLayout.CENTER);
        panelSearch.add(picLabel, BorderLayout.WEST);
        add(panelSearch, BorderLayout.NORTH);
        tableModel = new DefaultTableModel(new Object[][]{}, new String[]{"BOOKS"});
        table = new JTable(tableModel);
        table.setRowHeight(22);
        table.setFillsViewportHeight(true);
        table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        add(new JScrollPane(table), BorderLayout.CENTER);
    }

    public void showBooks() {
        Author author = listAuthors.get(row);
        long authorId = author.getId();
        try {
            String query = "SELECT book_title,isbn_number FROM book WHERE author_id = ?";

            PreparedStatement ps = Libproject.databaseConnection.prepareStatement(query);
            ps.setLong(1, authorId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                        Book book = new Book(rs.getString("book_title"), author, rs.getString("isbn_number"), 0);
                        tableModel.addRow(new Object[]{book.getTitle()});
                    }

        } catch (SQLException ex) {
            Logger.getLogger(DialogBooks.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
