/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import libproject.Author;
import libproject.Book;
import libproject.Libproject;
import libproject.MyObjectOutputStream;

/**
 *
 * @author jakata
 */
public final class DialogAddBook extends JDialog implements ActionListener {

    private JButton buttonsAdd;
    private JTextField bookTextField, isbnNumberTextfield;
    private JLabel labelTitle, labelIsbnNumber;
    private JComboBox comboBoxAuthors;
    private DefaultComboBoxModel comboBoxModelAuthors;
    private final DefaultTableModel tableModel;
    private final String filePath = "C:\\Users\\jakata\\Desktop\\authors.txt";
    List<Author> listAuthors = new ArrayList<>();
    String nameOfSelectedAuthor;
    List<Book> listBooks = new ArrayList<>();


    public DialogAddBook(List list,String title, JDialog parent, DefaultTableModel tableModel) {
        super(parent, true);
        this.tableModel = tableModel;
        listBooks=list;
        initComponents();
        setSize(500, 500);
        setResizable(false);
        setTitle(title);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    private void initComponents() {
        setLayout(new BorderLayout());
        buttonsAdd = new JButton("Save");
        comboBoxModelAuthors = new DefaultComboBoxModel();
        comboBoxAuthors = new JComboBox(comboBoxModelAuthors);

        bookTextField = new JTextField();
        isbnNumberTextfield = new JTextField();
        labelTitle = new JLabel();
        labelIsbnNumber = new JLabel();
        listAuthors = loadAuthorsFromDatabase();
        //Author[] array = listAuthors.toArray(new Author[listAuthors.size()]);

        for (int i = 0; i < listAuthors.size(); i++) {
            comboBoxModelAuthors.addElement(listAuthors.get(i));
        }

        buttonsAdd.addActionListener(this);

        JPanel southPanel = new JPanel();
        southPanel.add(buttonsAdd);
        add(southPanel, BorderLayout.SOUTH);
        JPanel centerPanel = new JPanel(new GridLayout(0, 1));
        centerPanel.add(new JLabel("Book Title"));
        centerPanel.add(bookTextField);
        centerPanel.add(labelTitle);
        centerPanel.add(new JLabel("Author"));
        centerPanel.add(comboBoxAuthors);
        centerPanel.add(new JLabel("ISBN Number"));
        centerPanel.add(isbnNumberTextfield);
        centerPanel.add(labelIsbnNumber);
        add(centerPanel, BorderLayout.CENTER);

    }

    public void saveBookToFile(Book book) {

        //Problem Description:
//Every time you serialize an object to a file, you only want to append the object 
//to the end of the file instead of overwriting it. 
//You can use FileInputStream (file nameOfSelectedAuthor, true); when reading data, 
//it will be read normally for the first time. It will report an error, and when it is read the second time,
//it will report a java.io.StreamCorruptedException: invalid type code: AC error.
//problem analysis:
//
//Since FileOutputStream (file nameOfSelectedAuthor, true) is used to serialize objects to the same file,
//a header is serialized to the file each time. When deserializing, 
//each ObjectInputStream object will only read one header, 
//then when encountering the second one, an error will be reported, resulting in an exception.
        try {

            File file = new File("C:\\Users\\jakata\\Desktop\\books.txt");
            FileOutputStream fileOutputStream = new FileOutputStream(file, true);

            if (file.length() < 1) {
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
                objectOutputStream.writeObject(book);
                objectOutputStream.close();

            } else {
                MyObjectOutputStream mos = new MyObjectOutputStream(fileOutputStream);
                mos.writeObject(book);
                mos.close();

            }

            //fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void saveBookToDatabase(Book book) {
        try {

            Connection connection = Libproject.databaseConnection;

            String bookTitle = book.getTitle();
            String isbnNumber = book.getIsbnNumber();
            long authorId = book.getAuthor().getId();
            String query = "INSERT INTO BOOK(book_Title,isbn_number,author_id) VALUES('" + bookTitle + "','" + isbnNumber + "','" + authorId + "')";

            Statement statement = connection.createStatement();
            statement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                long bookId = rs.getLong(1);
                book.setId(bookId);
                listBooks.add(book);
            }
           // connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(DialogAddAuthor.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public List<Author> loadAuthorsFromFile() throws IOException, ClassNotFoundException {

        FileInputStream fis = new FileInputStream(new File(filePath));

        try {
            listAuthors = new ArrayList<>();
            ObjectInputStream ois = new ObjectInputStream(fis);
            while (fis.available() > 0) {
                Author author = (Author) ois.readObject();
                listAuthors.add(author);
            }
            ois.close();

        } catch (FileNotFoundException ex) {
            Logger.getLogger(DialogAddBook.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DialogAddBook.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            fis.close();

        }
        return listAuthors;
    }

    public List<Author> loadAuthorsFromDatabase() {

        try {
            String query = "SELECT id,surname,last_name,phone FROM AUTHOR";
            Connection connection = Libproject.databaseConnection;
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                Author author = new Author(rs.getLong("id"), rs.getString("surname"), rs.getString("last_name"), rs.getString("phone"));
                listAuthors.add(author);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DialogManageAuthors.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listAuthors;

    }

    //This is called when button save book is clicked
    @Override
    public void actionPerformed(ActionEvent e) {
        String bookTitle = bookTextField.getText();
        //String author = authorTextfield.getText();
        String isbnNumber = isbnNumberTextfield.getText();
        Author selectedAuthor = (Author) comboBoxModelAuthors.getSelectedItem();
        System.out.println("surname = " + selectedAuthor.getSurname());

        if (!bookTitle.matches("(?i)(^[a-z]+)((?![ .,'-]$)[a-z .,'-]){0,28}$")) {

            bookTextField.setBorder(BorderFactory.createLineBorder(Color.RED));
            labelTitle.setForeground(Color.red);
            labelTitle.setText("Invalid name");
            return;

        } else {

            System.out.println("This Title is valid ");
            bookTextField.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
            labelTitle.setForeground(Color.BLACK);
            labelTitle.setText("valid");

            if (!isbnNumber.matches("\\d+")) {

                isbnNumberTextfield.setBorder(BorderFactory.createLineBorder(Color.RED));
                labelIsbnNumber.setForeground(Color.red);
                labelIsbnNumber.setText("Invalid Number");
                return;
            } else {
                System.out.println("This Number is valid ");
                isbnNumberTextfield.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
                labelIsbnNumber.setForeground(Color.BLACK);
                labelIsbnNumber.setText("valid");
                Book book = new Book(bookTitle, selectedAuthor, isbnNumber, 0);
                tableModel.addRow(new Object[]{book.getTitle(),
                    (book.getAuthor().getSurname() + " " + book.getAuthor().getLastName()),
                    book.getIsbnNumber()});                
                saveBookToFile(book);
                saveBookToDatabase(book);
            }

        }
        //validateUserInput(author);
        this.dispose();

    }

}
