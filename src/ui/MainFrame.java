/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.WindowConstants;

/**
 *
 * @author jakata
 */
public final class MainFrame extends JFrame {

    public MainFrame(String title) {
        super(title);
        initComponents();
        this.setExtendedState(this.getExtendedState() | JFrame.MAXIMIZED_BOTH);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLayout(new FlowLayout());
    }

    private void initComponents() {
       JMenuBar menu = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        fileMenu.add(new JMenuItem(new AbstractAction("Manage Books") {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogManageBooks dlg = new DialogManageBooks("Manage Books", MainFrame.this);
                dlg.setVisible(true);
            }
        }));
        fileMenu.add((new JMenuItem(new AbstractAction("Manage Authors") {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogManageAuthors dlg = new DialogManageAuthors("Manage Authors", MainFrame.this);
                dlg.setVisible(true);
            }
        })));
        fileMenu.add(new JMenuItem( new AbstractAction("Collections") {
           @Override
           public void actionPerformed(ActionEvent e) {
               
               DialogViewBooks dialogViewBooks = new DialogViewBooks( "View Books",MainFrame.this);
               dialogViewBooks.setVisible(true);
               
           }
        }));
        menu.add(fileMenu);
        setJMenuBar(menu);
    }

    
}
