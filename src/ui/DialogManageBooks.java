/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import libproject.Author;
import libproject.Book;
import libproject.HintTextField;
import libproject.Libproject;

/**
 *
 * @author jakata
 */
public final class DialogManageBooks extends JDialog {

    private JTable table;
    private DefaultTableModel tableModel;
    private JButton buttonAdd, buttonDelete, buttonEdit, buttonAscending, buttonDescending, btnSearch;
    private final String filePath = "C:\\Users\\jakata\\Desktop\\books.txt";
    private List<Book> listBooks = new ArrayList<>();
    ;
    private HintTextField htfSearch;

    public DialogManageBooks(String title, JFrame parent) {
        super(parent, true);

        initComponents();
        //if (loadBooksFromDatabase().size() > 0) {
        List<Book> books = loadBooksFromDatabase();
        books.forEach(b -> {
            if (b.getAuthor().getSurname() == null) {
                b.getAuthor().setSurname(" ");
                b.getAuthor().setLastName(" ");

            }
            tableModel.addRow(new Object[]{b.getTitle(), b.getAuthor().getSurname()
                + " " + b.getAuthor().getLastName(), b.getIsbnNumber()});
        });
        //}
        setSize(700, 500);
        setResizable(false);
        setTitle(title);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(null);
    }

    private void initComponents() {
        setLayout(new BorderLayout());
        //we already know that our dialog has a BorderLayout
        JPanel panelSearch = new JPanel(new BorderLayout());
        panelSearch.setBorder(new EmptyBorder(5, 5, 5, 5));
        panelSearch.setBackground(Color.MAGENTA);
        htfSearch = new HintTextField("Search book title");
        panelSearch.add(htfSearch, BorderLayout.CENTER);
        btnSearch = new JButton("Search Book");
        panelSearch.add(btnSearch, BorderLayout.EAST);
        add(panelSearch, BorderLayout.NORTH);
        //create table
        tableModel = new DefaultTableModel(new Object[][]{}, new String[]{"Title", "Author", "ISBN"}) {
            Class[] columnClasses = new Class[]{String.class, String.class, Integer.class};

            @Override
            public Class<?> getColumnClass(int columnIndex) {
                return columnClasses[columnIndex];
            }
        };

        table = new JTable(tableModel);
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    int index = table.getSelectedRow();
                    System.out.println("Selected row numbre " + (index + 1));
                }
            }
        });
        table.setRowHeight(22);
        table.setFillsViewportHeight(true);
        table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        add(new JScrollPane(table), BorderLayout.CENTER);
        //add bottom panel
        JPanel panelActions = new JPanel(new FlowLayout());
        buttonAdd = new JButton("Add");
        panelActions.add(buttonAdd);
        buttonDelete = new JButton("Delete");
        panelActions.add(buttonDelete);
        buttonEdit = new JButton("Edit");
        panelActions.add(buttonEdit);
        buttonAscending = new JButton("Asc");
        panelActions.add(buttonAscending);
        buttonDescending = new JButton("Dsc");
        panelActions.add(buttonDescending);

        add(panelActions, BorderLayout.SOUTH);

        buttonAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogAddBook dialogAddBook = new DialogAddBook(listBooks, "Add Book", DialogManageBooks.this, tableModel);
                dialogAddBook.setVisible(true);
                //loadBooksFromFile();
            }

        });

        buttonAscending.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tableModel.setRowCount(0);

                listBooks = loadBooksFromDatabase();
                Collections.sort(listBooks);
                for (Book b : listBooks) {
                    if (b.getAuthor().getSurname() == null) {
                        b.getAuthor().setSurname(" ");
                        b.getAuthor().setLastName(" ");

                    }
                    tableModel.addRow(new Object[]{b.getTitle(), b.getAuthor().getSurname() + " " + b.getAuthor().getLastName(), b.getIsbnNumber()});
                }
            }
        });

        buttonDescending.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tableModel.setRowCount(0);

                listBooks = loadBooksFromDatabase();
                Collections.sort(listBooks, Collections.reverseOrder());
                for (Book b : listBooks) {
                    if (b.getAuthor().getSurname() == null) {
                        b.getAuthor().setSurname(" ");
                        b.getAuthor().setLastName(" ");

                    }
                    tableModel.addRow(new Object[]{b.getTitle(), b.getAuthor().getSurname() + " " + b.getAuthor().getLastName(), b.getIsbnNumber()});
                }
            }
        });

        buttonDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int row = table.getSelectedRow();
                if (row == -1) {
                    JOptionPane.showMessageDialog(DialogManageBooks.this, "Please select a book first");
                    return;
                }

                row = table.getSelectedRow();
                String value = table.getModel().getValueAt(row, 2).toString();
                try {

                    String query = "DELETE FROM BOOK WHERE isbn_number =" + value;

                    Connection connection = Libproject.databaseConnection;
                    Statement statement = connection.createStatement();
                    statement.executeUpdate(query);
                    tableModel.removeRow(row);

                } catch (SQLException ex) {
                    Logger.getLogger(DialogManageAuthors.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        buttonEdit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int row = table.getSelectedRow();
                if (row == -1) {
                    JOptionPane.showMessageDialog(DialogManageBooks.this, "Please select a book first");
                    return;
                }
                Book book = listBooks.get(row);
                DialogEditBook dialogEditBook
                        = new DialogEditBook(book, "Edit Book", DialogManageBooks.this, tableModel, row);
                dialogEditBook.updateDialog();
                dialogEditBook.setVisible(true);

            }
        });

        btnSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tableModel.setRowCount(0);
                String value = htfSearch.getText();
                try {
                    String query = "SELECT b.book_title,b.isbn_number,a.surname AS surname,a.last_name AS last_name,a.phone AS phone,a.id AS id "
                            + "FROM book AS b  JOIN author AS a ON b.author_id=a.id WHERE book_title = ? ";
                    PreparedStatement ps = Libproject.databaseConnection.prepareStatement(query);
                    ps.setString(1, value);
                    ResultSet rs = ps.executeQuery();
                    while (rs.next()) {
                        Author author = new Author(rs.getLong("id"), rs.getString("surname"), rs.getString("last_name"), rs.getString("phone"));
                        Book book = new Book(rs.getString("book_title"), author, rs.getString("isbn_number"), 0);
                        tableModel.addRow(new Object[]{book.getTitle(), book.getAuthor().getSurname()
                            + " " + author.getLastName(), book.getIsbnNumber()});
                    }

                } catch (SQLException ex) {
                    Logger.getLogger(DialogManageAuthors.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

    }

    public ArrayList<Book> loadBooksFromFile() throws IOException {
//        FileInputStream fis = null;
//        ObjectInputStream ois = null;
//        ArrayList<Book> listBooks = new ArrayList<>();
//        try {
//            File file = new File(filePath);
//            file.createNewFile();
//            fis = new FileInputStream(filePath);
//            Book obj;
//            if (fis.available() < 0) {
//                ois = new ObjectInputStream(fis);
//                obj = (Book) ois.readObject();
//                listBooks.add(obj);
//
//            } else {
//                while (fis.available() > 0) {
//                    ois = new ObjectInputStream(fis);
//                    obj = (Book) ois.readObject();
//                    System.out.println(obj.getTitle() + obj.getAuthor() + obj.getIsbnNumber());
//                    listBooks.add(obj);
//                }
//                ois.close();
//            }
//            //In the while loop of deserialization, a new ObjectInputStream is created each time to read the header
//            
//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(DialogManageBooks.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (IOException | ClassNotFoundException ex) {
//            Logger.getLogger(DialogManageBooks.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            try {
//                fis.close();
//            } catch (IOException ex) {
//                Logger.getLogger(DialogManageBooks.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
        ArrayList<Book> listBooks = new ArrayList<Book>();

        File file = new File(filePath);
        //if this returns true that means the file is empty
        if (file.length() == 0) {

            return new ArrayList<>();
        }
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);
        while (fis.available() > 0) {
            Book book;
            try {
                book = (Book) ois.readObject();
                listBooks.add(book);

            } catch (ClassNotFoundException ex) {
                Logger.getLogger(DialogManageBooks.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        ois.close();
        fis.close();
        return listBooks;

    }

    public List<Book> loadBooksFromDatabase() {
        listBooks = new ArrayList<>();
        try {

            String query = "SELECT b.id,b.book_title,b.isbn_number,a.id AS author_id,"
                    + "a.surname AS surname,a.last_name AS last_name,a.phone AS phone "
                    + "FROM book AS b LEFT JOIN author AS a ON b.author_id=a.id ";

            Connection connection = Libproject.databaseConnection;
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(query);

            while (rs.next()) {
                Author author = new Author(rs.getLong("author_id"), rs.getString("surname"), rs.getString("last_name"), rs.getString("phone"));
                Book book = new Book(rs.getString("book_title"), author,
                        rs.getString("isbn_number"), rs.getLong("id"));

                listBooks.add(book);
            }

        } catch (SQLException ex) {
            Logger.getLogger(DialogManageAuthors.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listBooks;

    }

}
