/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libproject;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import ui.DialogManageAuthors;
import ui.MainFrame;

/**
 * Steps involved in connecting to a JDBC database
 *
 * Import java.sql package Load and register the driver --com.mysql.jdbc.Driver
 * Create connection Create a statement Executer the query Process the results
 * close
 *
 *
 */
public class Libproject {

    public static Connection databaseConnection;

    public static void main(String[] args) {
        try {
            String url = "jdbc:mysql://localhost:3306/library";
            String userName = "root";
            String passWord = "";
            Class.forName("com.mysql.jdbc.Driver");
            databaseConnection = DriverManager.getConnection(url, userName, passWord);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(DialogManageAuthors.class.getName()).log(Level.SEVERE, null, ex);
        }
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                MainFrame frame = new MainFrame("Sips Library");
                frame.setVisible(true);
            }
        });
//event dispatcher thread
    }

}
