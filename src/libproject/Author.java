/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libproject;

import java.io.Serializable;

//Autho a=new Author();
public class Author implements Serializable, Comparable<Author> {

    private String surname;
    private String lastName;
    private String phoneNumber;
    private long id;
    
     public Author(long id, String surname, String lastName, String phoneNumber) {
        this.surname = surname;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.id = id;
    }


    public String getLastName() {
        
       
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
      public String getSurname() {
        
          
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
     public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


   
  

   

    @Override
    public String toString() {
        return surname + ' ' + lastName;
    }

    

   
    @Override
    public int compareTo(Author a) {
        return this.getSurname().compareTo(a.getSurname());
    }

}
