/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libproject;

import java.io.Serializable;

/**
 *
 * @author jakata
 */
public final class Book implements Serializable, Comparable<Book> {

    private String title;
    private String isbnNumber;
    private Author author;
    private long id;

    public Book(String title, Author author, String isbnNumber, long bookId) {

        this.title = title;
        this.isbnNumber = isbnNumber;
        this.author = author;
        this.id = bookId;

    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Author getAuthor() {
        return author;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getTitle() {

        return title;
    }

    public String getIsbnNumber() {
        return isbnNumber;
    }

    public void setIsbnNumber(String isbnNumber) {
        this.isbnNumber = isbnNumber;
    }

    @Override
    public int compareTo(Book b) {
        return this.getTitle().compareTo(b.getTitle());
    }

}
